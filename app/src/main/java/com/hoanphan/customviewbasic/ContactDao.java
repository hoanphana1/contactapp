package com.hoanphan.customviewbasic;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.hoanphan.customviewbasic.Model.Contact;

import java.util.List;

@Dao
public interface ContactDao {

    @Query("SELECT * FROM Contact")
    List<Contact> getAllContact();

    @Insert
    void insertContact(Contact... contacts);

    @Delete
    void delete(Contact contact);

    @Query("DELETE FROM Contact")
    void deleteAll();


}
