package com.hoanphan.customviewbasic.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hoanphan.customviewbasic.Model.Contact;
import com.hoanphan.customviewbasic.R;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private static ArrayList<Contact> mContact ;
    private static Context mContext;

    public CustomAdapter(ArrayList<Contact> mContact, Context mContext) {
        this.mContact = mContact;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_listview, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvName.setText(mContact.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return mContact.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvName;
        public View view;
        public SearchView svName;
        public LinearLayout layoutInfo;
        public TextView tvCall;
        public MyViewHolder(View v) {
            super(v);
            view = v;
            tvName = view.findViewById(R.id.tv_name);
            svName = view.findViewById(R.id.sv_name);
            layoutInfo = view.findViewById(R.id.Layout_info);
            tvCall = view.findViewById(R.id.tv_call);
            tvName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(layoutInfo.getVisibility()==view.GONE){
                        layoutInfo.setVisibility(View.VISIBLE);
                    }else{
                        layoutInfo.setVisibility(View.GONE);
                    }
                }
            });
            tvCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position= getAdapterPosition();
                    String phone = mContact.get(position).getMobile();
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    mContext.startActivity(intent);
                }
            });
        }
    }
}