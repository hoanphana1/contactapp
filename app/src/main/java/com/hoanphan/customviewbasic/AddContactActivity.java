package com.hoanphan.customviewbasic;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.hoanphan.customviewbasic.Model.Contact;

public class AddContactActivity extends AppCompatActivity {

    private EditText edtName,edtEmail,edtPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        edtName =findViewById(R.id.edt_name);
        edtEmail =findViewById(R.id.edt_email);
        edtPhone =findViewById(R.id.edit_mobile);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mi_done:
                Contact mContact = new Contact(1,edtName.getText().toString(),edtEmail.getText().toString(),edtPhone.getText().toString());
                Intent intent = new Intent();
                intent.putExtra("contact",mContact);
                setResult(RESULT_OK,intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}