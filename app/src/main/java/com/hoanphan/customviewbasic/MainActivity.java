package com.hoanphan.customviewbasic;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.SearchView;

import com.hoanphan.customviewbasic.Model.Contact;
import com.hoanphan.customviewbasic.adapter.CustomAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Filterable {
    private RecyclerView rvContacts;
    private  ArrayList<Contact> arrayList;
    private ArrayList<Contact> contactListFiltered;
    private CustomAdapter myAdapter;
    private SearchView svName;

    private  ContactDatabase contactDatabase;
    private ContactDao contactDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvContacts = findViewById(R.id.rv_contacts);
        svName = findViewById(R.id.sv_name);
        rvContacts.setLayoutManager(new LinearLayoutManager(this));
        arrayList = new ArrayList<Contact>();

        arrayList.add(new Contact(1,"Phan Văn Hoan","012356789","adsa.com"));
        arrayList.add(new Contact(2,"Bùi Duy Nam","012358249","asdasd.com"));
        arrayList.add(new Contact(3,"Trần Như Khánh","012396969","kkfkfkf.com"));
        arrayList.add(new Contact(4,"Lê Nam Trung","012396945","asdasd.com"));

        myAdapter = new CustomAdapter(arrayList,this);
        rvContacts.setAdapter(myAdapter);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                contactDatabase = ContactDatabase.getInstance(getApplicationContext());
                contactDao = contactDatabase.contactDao();
                List<Contact> dbContact = contactDao.getAllContact();
                for (Contact contact: dbContact){
                    arrayList.add(contact);
                }
                myAdapter.notifyDataSetChanged();
            }
        });


        svName.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                getFilter().filter(s);
                return true;
            }
        });
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = arrayList;
                } else {
                    ArrayList<Contact> filteredList = new ArrayList<>();
                    for (Contact row : arrayList) {

                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getMobile().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<Contact>) filterResults.values;
                // refresh the list with filtered data
                 myAdapter = new CustomAdapter(contactListFiltered, null);
                rvContacts.setAdapter(myAdapter);
            }
        };
    }
    public void clickOnAddContact(View v){
        Intent intent = new Intent(MainActivity.this,AddContactActivity.class);
        startActivityForResult(intent,567);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode == RESULT_OK && requestCode == 567){
            final Contact newContact =(Contact) data.getSerializableExtra("contact");
            arrayList.add(newContact);
            myAdapter.notifyDataSetChanged();

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    contactDao.insertContact(newContact);
                }
            });
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}